// ==UserScript==
// @name         WhatsApp Full HD Fix
// @namespace    Violentmonkey Scripts
// @version      0.0.1
// @description  WhatsApp Web blank space remover
// @author       lucasenl
// @match        *://*web.whatsapp.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=web.whatsapp.com
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    let styles = `
        body #app .app-wrapper-web .two, body #app .app-wrapper-web .three {
            height: 100%;
            top: 0;
            width: 100%;
            max-width: 100%;
        }

        @media screen and (min-width: 1441px) {
            html[dir] .app-wrapper-web ._1jJ70 {
                margin: 0 auto;
                box-shadow: 0 6px 18px rgba(var(--shadow-rgb),.05);
            }

            .app-wrapper-web ._1jJ70 {
                top: 19px;
                width: calc(100% - 38px);
                max-width: 1600px;
                height: calc(100% - 38px);
            }
        }
    `;

    let styleSheet = document.createElement("style");
    styleSheet.type = "text/css";
    styleSheet.innerText = styles;
    document.head.appendChild(styleSheet);
})();
