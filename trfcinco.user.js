// ==UserScript==
// @name         Hide TRF5 Popup
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Removes the pop-up on the TRF5 website, identified by ID.
// @author       lucsenl
// @match        *://certidoes.trf5.jus.br/*
// @match        *://*.certidoes.trf5.jus.br/*
// @grant        none
// @downloadURL  https://gitlab.com/lucsenl/userscript/-/raw/main/trfcinco.user.js
// @updateURL    https://gitlab.com/lucsenl/userscript/-/raw/main/trfcinco.user.js
// ==/UserScript==

(function() {
    'use strict';

    // Função para remover o elemento assim que ele aparecer
    function removeElement() {
        const targetElement = document.getElementById('dialogCjfDadosConsultados');
        if (targetElement) {
            targetElement.remove();  // Remove o elemento completamente
        }
    }

    // Inicialização do MutationObserver para observar mudanças no DOM
    function initObserver() {
        const observer = new MutationObserver(mutations => {
            mutations.forEach(mutation => {
                if (mutation.type === 'childList') {
                    removeElement();  // Remove o elemento se for detectado em qualquer mudança
                }
            });
        });

        // Configura o observer para observar mudanças na árvore de elementos
        observer.observe(document.body, { childList: true, subtree: true });
    }

    // Remove imediatamente se o elemento já estiver presente
    removeElement();

    // Inicializa o observer
    initObserver();
})();