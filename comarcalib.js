// ==UserScript==
// @name         comarcalib
// @namespace    Violentmonkey Scripts
// @grant        none
// @version      0.1
// @license      MIT
// @description  Library to select the comarca automatically
// ==/UserScript==
 
(function() {
    'use strict';
 
    if (window.automacaoExecutada) {
        return;
    }
 
    window.automacaoExecutada = true;
 
    function clickElement(element) {
        var event = new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
            view: window
        });
        element.dispatchEvent(event);
    }
 
    function selectOption(selectElement, index) {
        var option = selectElement.querySelector("option:nth-child(" + index + ")");
        if (option) {
            option.selected = true;
            var event = new Event('change', {
                bubbles: true,
                cancelable: true
            });
            selectElement.dispatchEvent(event);
        }
    }
 
    var numCliques = 0;
 
    if (window.location.href.startsWith("https://archivum.mprn.mp.br/webfrmp/")) {
        window.addEventListener('load', function() {
            if (window.location.href === "https://archivum.mprn.mp.br/webfrmp/default.aspx") {
                numCliques++;
                if (numCliques === 2) {
                    return;
                }
            } else {
                var cbComarca = document.getElementById('cbComarca');
                if (cbComarca) {
                    clickElement(cbComarca);
                    setTimeout(function() {
                        selectOption(cbComarca, 10);
                    }, 1000);
                }
            }
        });
    }
})();