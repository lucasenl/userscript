// ==UserScript==
// @name           Tweaks SuaMusica
// @namespace      Violentmonkey Scripts
// @match          *://*suamusica.com.br/*
// @match          *://*suamusica.com/*
// @version        0.0.1
// @grant          GM_addStyle
// @icon           https://www.google.com/s2/favicons?sz=64&domain=suamusica.com.br
// ==/UserScript==

(function() {
    'use strict';

    GM_addStyle('.styles_container__EcRzX { height: 50px !important; }');
    GM_addStyle('.styles_MenuDesktopContainer__rgf6w { height: 33px !important; }');
    GM_addStyle('.styles_imageContainer__FrBJY > span:nth-child(1) > img:nth-child(2) { min-width: 85% !important; min-height: 85% !important; }');
    GM_addStyle('[id^="Suamusica\\.com\\.br-ROS-Top-Leaderboard-"] { padding-top: 19px !important; }');
    GM_addStyle('[id^="Suamusica\.com\.br-ROS-Top-Leaderboard-"] { padding-top: 19px !important; }');
    GM_addStyle('.styles_feedContainer__k1oZf { margin-top: -42px !important; }');
    GM_addStyle('div.styles_ArtistVideoContainer__8rSwY:nth-child(2) { display: none !important; }');
})();
