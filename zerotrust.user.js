// ==UserScript==
// @name         Tweaks Zero Trust
// @namespace    Violentmonkey Scripts
// @version      1.3
// @description  Hide specific items on one.dash.cloudflare.com
// @icon         https://www.google.com/s2/favicons?sz=64&domain=one.dash.cloudflare.com
// @match        *://one.dash.cloudflare.com/*
// @grant        none
// @run-at       document-end
// @license      MIT
// @downloadURL  https://gitlab.com/lucsenl/userscript/-/raw/main/zerotrust.user.js
// @updateURL    https://gitlab.com/lucsenl/userscript/-/raw/main/zerotrust.user.js
// ==/UserScript==

(function() {
    'use strict';

    function hideElements() {
        if (!window.location.href.includes('/home')) {
            return;  // Exit if not on the /home page
        }

        const selectors = [
            'html > body > div > div:nth-of-type(1) > main > div:nth-of-type(2) > div > div:nth-of-type(2)', // Elemento que você já tinha
            'html > body > div > div:nth-of-type(1) > main > div:nth-of-type(2) > div > div:nth-of-type(3) > div > div > button:nth-of-type(3)', // Novo seletor para o botão 3
            'html > body > div > div:nth-of-type(1) > main > div:nth-of-type(2) > div > div:nth-of-type(3) > div > div > button:nth-of-type(4)'  // Novo seletor para o botão 4
        ];

        // Ocultar elementos
        selectors.forEach(selector => {
            const elements = document.querySelectorAll(selector);
            elements.forEach(el => {
                el.style.display = 'none'; // Usando JavaScript puro para ocultar elementos
                console.log(`Hiding element: ${selector}`);
            });
        });

        // Adicionar margin-bottom ao elemento específico
        const targetSelector = 'html > body > div > div:nth-of-type(1) > main > div:nth-of-type(2) > div > div:nth-of-type(1)';
        const targetElements = document.querySelectorAll(targetSelector);
        targetElements.forEach(el => {
            el.style.marginBottom = '20px'; // Adicionando margin-bottom
            console.log(`Setting margin-bottom: 20px to element: ${targetSelector}`);
        });
    }

    function init() {
        hideElements();  // Hide elements initially
        const observer = new MutationObserver((mutationsList) => {
            for (const mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    hideElements();  // Re-hide elements if the DOM changes
                }
            }
        });

        observer.observe(document.body, { childList: true, subtree: true });

        // Check periodically in case of late DOM updates
        setInterval(hideElements, 1000);
    }

    window.addEventListener('load', init);
})();
