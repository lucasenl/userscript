// ==UserScript==
// @name         Youtube for Privacy
// @namespace    Violentmonkey Scripts
// @version      0.0.1
// @author       ENL
// @match        *://*.youtube.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=youtube.com
// @grant        none
// @run-at       document-start
// @license      MIT
// ==/UserScript==

(function() {
    'use strict';

    if (window.location.href.match(/^https?:\/\/(?:www\.)?youtube\.com/)) {
        var newUrl = window.location.href.replace(/^https?:\/\/(?:www\.)?youtube\.com/, 'https://inv.zzls.xyz');
        window.location.replace(newUrl);
    }
})();
