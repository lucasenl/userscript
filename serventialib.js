// ==UserScript==
// @name         serventialib
// @namespace    Violentmonkey Scripts
// @grant        none
// @version      1.0
// @license      MIT
// @description  Library to select the serventia automatically
// ==/UserScript==
 
(function() {
    'use strict';
 
    if (window.segundoPassoExecutado) {
        return;
    }
 
    window.segundoPassoExecutado = true;
 
    var cbSecretaria = document.getElementById('cbSecretaria');
    if (cbSecretaria) {
        var option = cbSecretaria.querySelector('option[value="39"]');
        if (option) {
            option.selected = true;
        }
    }
})();