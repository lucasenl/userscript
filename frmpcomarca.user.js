// ==UserScript==
// @name         Seletor Comarca FRMP 1/2
// @namespace    Violentmonkey Scripts
// @match        *://archivum.mprn.mp.br/*
// @grant        none
// @author       lucsenl
// @version      1.0
// @license      MIT
// @icon         https://www.mprn.mp.br/wp-content/uploads/2022/05/cropped-Novo-FAVICON-Portal-do-MPRN-32x32.png
// @downloadURL  https://gitlab.com/lucsenl/userscript/-/raw/main/frmpcomarca.user.js
// @updateURL    https://gitlab.com/lucsenl/userscript/-/raw/main/frmpcomarca.user.js
// ==/UserScript==

(function() {
    'use strict';

    if (window.automacaoExecutada) {
        return;
    }

    window.automacaoExecutada = true;

    function clickElement(element) {
        var event = new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
            view: window
        });
        element.dispatchEvent(event);
    }

    function selectOption(selectElement, index) {
        var option = selectElement.querySelector("option:nth-child(" + index + ")");
        if (option) {
            option.selected = true;
            var event = new Event('change', {
                bubbles: true,
                cancelable: true
            });
            selectElement.dispatchEvent(event);
        } else {
            console.error('Opção não encontrada no elemento select');
        }
    }

    var numCliques = 0;

    if (window.location.href.startsWith("https://archivum.mprn.mp.br/webfrmp/")) {
        window.addEventListener('load', function() {
            if (window.location.href === "https://archivum.mprn.mp.br/webfrmp/default.aspx") {
                numCliques++;
                if (numCliques === 2) {
                    return;
                }
            } else {
                var cbComarca = document.getElementById('cbComarca');
                if (cbComarca) {
                    clickElement(cbComarca);
                    setTimeout(function() {
                        selectOption(cbComarca, 10);
                    }, 1000);
                } else {
                    console.error('Elemento #cbComarca não encontrado');
                }
            }
        });
    }
})();