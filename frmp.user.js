// ==UserScript==
// @name          Tweaks FRMP
// @version       0.0.1
// @namespace     lucsenl
// @description   Small adjustments to the FRMP.
// @author        lucsenl
// @include       *://frmp.mprn.mp.br/public/gerar-boleto
// @match         *://frmp.mprn.mp.br/public/gerar-boleto
// @run-at        document-end
// @require       https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js
// @icon          https://www.mprn.mp.br/wp-content/uploads/2022/05/cropped-Novo-FAVICON-Portal-do-MPRN-32x32.png
// @license       0BSD
// @copyright     2025, lucsenl
// @updateURL     https://gitlab.com/lucsenl/userscript/-/raw/main/frmp.user.js
// @downloadURL   https://gitlab.com/lucsenl/userscript/-/raw/main/frmp.user.js
// ==/UserScript==

(function () {
    'use strict';

    // Função para aplicar o estilo de padding e cor
    function aplicarEstilo() {
        // Aplicar padding nos headers
        $('[id^="mat-expansion-panel-header-"]').css({
            'padding-bottom': '27px',
            'padding-top': '27px'
        });

        // Aplicar cor preta para todos os títulos de painel
        $(".mat-expansion-panel-header-title").css('color', 'rgb(0, 0, 0)');
    }

    // Aplicar o estilo imediatamente
    $(document).ready(aplicarEstilo);

    // Monitorar mudanças no DOM e aplicar em elementos dinâmicos
    const observer = new MutationObserver(() => aplicarEstilo());

    observer.observe(document.body, { childList: true, subtree: true });

})();