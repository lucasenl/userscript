// ==UserScript==
// @name         Seletor Serventia FRMP 2/2
// @namespace    Violentmonkey Scripts
// @match        *://archivum.mprn.mp.br/*
// @grant        none
// @author       lucsenl
// @version      1.0
// @license      MIT
// @icon         https://www.mprn.mp.br/wp-content/uploads/2022/05/cropped-Novo-FAVICON-Portal-do-MPRN-32x32.png
// @downloadURL  https://gitlab.com/lucsenl/userscript/-/raw/main/frmpserventia.user.js
// @updateURL    https://gitlab.com/lucsenl/userscript/-/raw/main/frmpserventia.user.js
// ==/UserScript==

(function() {
    'use strict';

    if (window.segundoPassoExecutado) {
        return;
    }

    window.segundoPassoExecutado = true;

    var cbSecretaria = document.getElementById('cbSecretaria');
    if (cbSecretaria) {
        var option = cbSecretaria.querySelector('option[value="39"]');
        if (option) {
            option.selected = true;
        } else {
            console.error('Opção desejada não encontrada no elemento select');
        }
    } else {
        console.error('Elemento #cbSecretaria não encontrado');
    }
})();